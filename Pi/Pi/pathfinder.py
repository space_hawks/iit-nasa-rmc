from random import randint
import time

start = time.time()                                                     # set timer

# grid is A-H, 1-14 (for arrays 0-7 and 0-13)
# robot will start in 1-4
# navigation zone is 5-10
# goal area is 11-14

# this does not have very good "resolution" in that one obstacle = ONLY one square but with 5 obstacles and 8 columns
# there will necessarily  be a trivial solution since there are 3 unused columns

# the final program should ignore A, H, 1, and 14 as they are the "short" sectors 

def randomizer():

    # for testing, creates random obstacles and places robot

    locs = []                                                           # return array
    
    for i in range(5):
        locs.append((randint(0,7),randint(4,9)))                        # random obstacle locations cells 0-4
    locs.append((randint(0,7),0))                                       # final cell (5) in array is robot starting location

    return locs                                         

def open_columns(field):

    # find all open paths between start and mining areas
                                   
    blocked = set()                                                         # blank set of blocked

    for i in field:

        if i != field[5]:                                                   # add obstacles but not robot location
            blocked = blocked | {i[0]}
        else:
            return set(range(8)) - blocked                                  # return open columns

def find_closest(field):

    # find the column closest to the robot's current column
    # we can remove/disable print statements later

    closer = 1                                                              # counter for location
    opens = open_columns(field)                                             # open columns
    pt = field[5][0]                                                        # column of robot

    while 1:                                                                # this loop always returns something eventually

        if opens & {pt} == set():                                           # if there is not a straight shot

            if {pt + closer} & opens != set():                              # if X to the right is an open column

                # TODO: if both pick closest to center to ease line-up for dump
                
                print("Move robot right",closer,"space(s)")                 # open space is to the right

                return pt + closer                                          

            elif {pt - closer} & opens != set():
                print("Move robot left",closer,"space(s)")                  # open space is to the left

                return pt - closer

            else:
                closer += 1                                                 # don't have to worry about going past
                                                                            # since not an array
        else:                                                               # it's a straight shot

            print("Robot has a clear path")

            return pt


# input
field = randomizer()                                                        # generate random testing locations
#field = [(1, 7), (3, 6), (4, 4), (6, 4), (7, 6), (7, 0)]                   # or input specific via an array

#output
print("Obstacles and Robot Location",field)                                 # locations 0-4 are obstacles 5 is robot
print("")
find_closest(field)                                                         # feed finder locations
print(time.time() - start)
