import math
import cv2
import numpy
import time

start = time.time()                                         # start timer

rocks = []                                                  # global rock/pit counters
pits = []
rock_sectors = set()                                        # for output
pit_sectors = set()
                                                            # grid coords (more to come)
# list invisible sectors as well
                                                            
a5 = [(0,83),(45,83),(72,55),(0,55)]                        # just did these for test case
b5 = [(46,83),(175,83),(184,55),(73,55)]
b7 = [(95,34),(190,34),(195,20),(110,20)]
c8 = [(196,20),(275,20),(270,10),(200,10)]
d8 = [(276,19),(360,19),(345,10),(271,10)]
e7 = [(375,34),(470,34),(440,20),(360,20)]
f8 = [(440,19),(479,19),(479,10),(420,10)]

def sectorize(points):                                      # locates in which sector a point lies

    sectors = []                                            # return array

    for i in points:
        
        if searchit(a5,(i[0],i[1])):                        # have to go through all because
            sectors.append("a5")                            # stuff can be in more than one sector
        elif searchit(b5,(i[0],i[1])):
            sectors.append("b5")
        elif searchit(b7,(i[0],i[1])):
            sectors.append("b7")
        elif searchit(c8,(i[0],i[1])):
            sectors.append("c8")
        elif searchit(d8,(i[0],i[1])):
            sectors.append("d8")
        elif searchit(e7,(i[0],i[1])):
            sectors.append("e7")
        elif searchit(f8,(i[0],i[1])):
            sectors.append("f8")
        else:
            sectors.append("X")                             # location on grid not yet mapped
                                                            # returns as false positive as well

    return set(sectors)

def searchit(coords,pt):                                    # find if a point is in an area (polygon)

    bl = coords[0]                                          # breaks the polygon into two triangles
    br = coords[1]                                          # and a rectangle
    tr = coords[2]
    tl = coords[3]
    lr = (tl[0],bl[1])
    rr = (br[0],tr[1])

    lt = [(bl,lr,tl)]
    rt = [(br,tr,rr)]
    rect = [(lr,br,rr,tl)]

    # may have to swap xy

    # can condense below with above                         

    if in_triangle(pt[0],pt[1],bl[0],bl[1],lr[0],lr[1],tl[0],tl[1]) or \
       in_triangle(pt[0],pt[1],br[0],br[1],tr[0],tr[1],rr[0],rr[1]) or \
        in_rectangle((pt[0],pt[1]),((lr[0],lr[1]),(br[0],br[1]),(rr[0],rr[1]),(tl[0],tl[1]))):
        return True
 
    else:
        return False

def find_maxmin(coords):

    ret = [None] * 4            # return array
    
    ret[0] = 0                  # max x
    ret[1] = 9999               # min x
    ret[2] = 0                  # max y
    ret[3] = 9999               # min y

    for i in coords:            # do the things

        if i[0] > ret[0]:
            ret[0] = i[0]

        if i[0] < ret[1]:
            ret[1] = i[0]

        if i[1] > ret[2]:
            ret[2] = i[1]

        if i[1] < ret[3]:
            ret[3] = i[1]

    return ret  


def in_rectangle(pt,coords):                            # find if a point is in a rectangle

    maxmin = find_maxmin(coords)            

    if pt[0] < maxmin[0] and pt[0] > maxmin[1] and pt[1] < maxmin[2] and pt[1] > maxmin[3]:
        return True


def tri_area(x1,y1,x2,y2,x3,y3):                        # find the area of a triangle
    return abs((x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))/2)

def in_triangle(x,y,x1,y1,x2,y2,x3,y3):                 # find if a point is in a triangle
    abc = (tri_area(x1,y1,x2,y2,x3,y3))
    pbc = (tri_area(x,y,x2,y2,x3,y3))
    apc = (tri_area(x1,y1,x,y,x3,y3))
    abp = (tri_area(x1,y1,x2,y2,x,y))
    total = (abc - (pbc+apc+abp))

    if total == 0:
        return True
    else:
        return False

def get_dist(x1,y1,x2,y2):                              # find the distance between two points
    newx = pow((x2-x1),2)
    newy = pow((y2-y1),2)
    return math.sqrt(newx + newy)

def determine_robot_location():

    # find red and green beacons
    # determine angle between (t=o/a)
    # that is direction robot is facing

    # don't forget DROP DOWN

    pass

class cluster:
    def __init__(self,x,y):
        self.points = [(x,y)]

def check_cluster(x,y,type):

    # don't need type if you pass the array (ie array not global)

    hit = 0

    if type == 'r':

        if len(rocks) == 0:
            rocks.append(cluster(y,x))
        else:
            for i in rocks:

                if get_dist(x,y,i.points[0][1],i.points[0][0]) < 35:            # this will change with revision
                    i.points.append((y,x))
                    hit += 1

            if hit == 0:
                rocks.append(cluster(y,x))

    else:
        #it's a pit
        
        if len(pits) == 0:
            pits.append(cluster(y,x))
        else:
            for i in pits:

                if get_dist(x,y,i.points[0][1],i.points[0][0]) < 60:            # as will this (higher rez)
                    i.points.append((y,x))
                    hit += 1

            if hit == 0:
                pits.append(cluster(y,x))



def file_load(infile):                                      # busted temp file loader
    
    img_bw = cv2.imread(infile,0)
    #img_color = cv2.imread(infile,1)                        # color version
    #b,g,r = cv2.split(img_color)                            # split into rgb (bgr in this case)

    #out_image = img_bw[420:503]

    #cv2.imwrite('testing3_crop.jpg',navigation_area)       # output file

    return img_bw

def determine_obstacle_location(infile):

    # find locations on the grid - partially implemented
    # derive how much each obstacle intrudes on each sector - TBD

    navigation_area = file_load(infile)[420:503]

    pitbit = 0
    rockbit = 0
    elsebit = 0
    indux = 0

    checker = 50                                    # don't have to check bottom 50
    maxcheck = navigation_area.max() - 50           # or top 50
    checkercount = 0

    rockstart = 0
    pitstart = 0

    # take previously tried threshold numbers and adjust as recommended



    


    cropped_area = navigation_area.shape[0] * navigation_area.shape[1]



    while checker < navigation_area.max():                      # this checks for range

        far_threshhold = .96
        pit_threshold = .87

        
        
        for (x),element in numpy.ndenumerate(navigation_area):

            if element > checker and element < maxcheck:         # for division point-ignore high/low end

                checkercount += 1
                
        if checkercount == 0:                             # found rock min
            
            rockstart = checker

            checker = 256                                   # exit loop

        elif checkercount >= cropped_area * .90:                         # > 96%-skip ahead
            checker += 5
        
        elif checkercount < cropped_area * .89:                            #for this image, 87% of total pxls

            if pitstart == 0:                               # for threshhold for pits

   
                print(checkercount,cropped_area)

                
                pitstart = checker


            # at this point if there are too many "pit points",
            # we need to re-adjust. but we don't know that until after we get rockstart            



            checker += 2                                    # skip ahead (less processing time)

        else:
            checker += 1
            
        checkercount = 0            


    # this produces an output image redundant code with above but was being lazy. will fix later.

    for (x,y),element in numpy.ndenumerate(navigation_area):   # for output image

        if not in_triangle(y,x,0,0,0,52,87,0):    # y,x for some fucked reason

            if element > 100 and element < pitstart:         # ignore low end < 100
        
                pitbit += 1
                numpy.put(navigation_area,[indux],[0])
                check_cluster(x,y,'p')

            elif element > rockstart and element < navigation_area.max():     # rocks
                rockbit += 1
                numpy.put(navigation_area,[indux],[255])
                check_cluster(x,y,'r')

            else:
                elsebit += 1
                numpy.put(navigation_area,[indux],[100])   # rest 150-200

        else:

            numpy.put(navigation_area,[indux],[50])    # ignore area

        indux += 1








    # refine arrays - make into def with further refinements

    pits2 = []
    rocks2 = []


    for i in rocks:
        if len(i.points) > 100:         # filter stuff that's too small

            rocks2.append(i.points)

    for j in pits:
        if len(j.points) > 100:             # filter stuff that's too small
            
            pits2.append(j.points)


    # a high number of pits means a lot of noise, re-run



    # take each coord in rocks2 and pits2 and add the sector to the appropriate list

    for i in rocks2:
        global rock_sectors
        rock_sectors = rock_sectors | sectorize(i)

    for i in pits2:
        global pit_sectors
        pit_sectors = pit_sectors | sectorize(i)









    cv2.imshow('output',navigation_area)            # output image
    

    # Below are diagnostics 

    print("Pits before filter")

    for i in pits:
        print(len(i.points),i.points[0])
        
##    print("Rocks before filter")
##
##    for j in rocks:
##        print(len(j.points),j.points[0])
##        
    print("pits after filter")

    for i in pits2:
        r2 = find_maxmin(i)
        print(len(i),i[0],r2)
        #print(r2[0]-r2[1],r2[2]-r2[3])
        
##    print("rocks after filter")


##    for i in rocks2:
##        r2 = find_maxmin(i)
##        print(len(i),i[0],r2)
##        #print(r2[0]-r2[1],r2[2]-r2[3])



determine_obstacle_location('testing.jpg')
print("Rocks: ",rock_sectors)
print("Pits: ",pit_sectors)
print("Time: ",time.time() - start)                      # elapsed time
